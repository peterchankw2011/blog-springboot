package blog.member;

import blog.exception.UserAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class MemberService implements IMemberService {
    @Autowired
    private MemberRepository memberRepository;

    @Override
    public Member createNewMember(MemberDto memberDto) throws UserAlreadyExistException {
        if(emailExist(memberDto.getEmail())){
            System.out.println("Email exist");
            throw new UserAlreadyExistException("There is an account with that email address: "
                    + memberDto.getEmail());
        }

        Member member = new Member();
        member.setPreferredName(memberDto.getPreferredName());
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        member.setPassword(passwordEncoder.encode(memberDto.getPassword()));
        member.setEmail(memberDto.getEmail());
        System.out.println("Before saving");
        return memberRepository.save(member);
    }

    private boolean emailExist(String email){
        return  memberRepository.findByEmail(email).isPresent();
    }

    public Member getMemberByEmail(String email){
        return memberRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Cannot find user."));
    }

}
