package blog.member;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Arrays;

public enum MemberAuthority {
    ADMIN, NORMAL;

    @JsonCreator
    public MemberAuthority fromString(String key) {
        return Arrays.stream(values())
                .filter(value -> value.name().equalsIgnoreCase(key))
                .findFirst()
                .orElse(null);
    }
}
