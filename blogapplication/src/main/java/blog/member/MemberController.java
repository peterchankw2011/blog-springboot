package blog.member;

import blog.exception.UserAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class MemberController {
    @Autowired
    MemberService memberService;

    @GetMapping("/register")
    public String showRegistrationFrom(WebRequest request, Model model){
        MemberDto memberDto = new MemberDto();
        model.addAttribute("member", memberDto);
        return "registration";
    }

    @PostMapping("/register")
    public ModelAndView registerMemberAccount(@ModelAttribute("member") @Valid MemberDto memberDto,
                                              HttpServletRequest request,
                                              Errors errors){

        System.out.println("Registration of Member: " + memberDto.getEmail());
        try {
            Member registeredMember = memberService.createNewMember(memberDto);
            System.out.println("Registration success: " + memberDto.getEmail());
        } catch(UserAlreadyExistException e){
            ModelAndView mav = new ModelAndView("registration", "member", memberDto);
            mav.addObject("message", "An account for that username/email already exists.");
            System.out.println("Registration fail: " + memberDto.getEmail());
            return  mav;
        }

        return new ModelAndView("welcome", "member", memberDto);
    }

}
