package blog.member;

import blog.exception.UserAlreadyExistException;

public interface IMemberService {
    Member createNewMember(MemberDto memberDto) throws UserAlreadyExistException;
}
