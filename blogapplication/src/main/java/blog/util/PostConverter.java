package blog.util;

import blog.post.Post;
import blog.post.PostRequest;
import blog.post.PostResponse;

public class PostConverter {

    private PostConverter(){

    }

    public static PostResponse toPostResponse(Post post){
        PostResponse response = new PostResponse();
        response.setId(post.getId());
        response.setTitle(post.getTitle());
        response.setContent(post.getContent());
        return response;
    }

    public static Post toPost(PostRequest request){
        Post post = new Post();
        post.setTitle(request.getTitle());
        post.setContent(request.getContent());
        return post;
    }
}
