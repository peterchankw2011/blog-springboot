package blog.util;

import blog.member.MemberDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        MemberDto member = (MemberDto) obj;
        return member.getPassword().equals(member.getMatchingPassword());
    }
}