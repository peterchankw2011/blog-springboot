package blog.post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PostRepository extends JpaRepository<Post, String> {
    @Modifying
    @Query("update Post p set p.title = :title, p.content = :content where p.id = :id")
    void setPostById(@Param(value="title") String title, @Param(value="content") String content,@Param(value="id")  String id);
}
