package blog.post;

import blog.auth.UserIdentity;
import blog.exception.NotFoundException;
import blog.util.PostConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostService {
    @Autowired
    private UserIdentity userIdentity;

    @Autowired
    PostRepository postRepository;

    public PostResponse createPost(PostRequest request) {
        Post post = PostConverter.toPost(request);
        post.setMemberId(userIdentity.getId());
        post = postRepository.save(post);
        return PostConverter.toPostResponse(post);
    }
    public Post getPost(String id){
        return postRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Can't find post"));
    }

    public PostResponse getPostResponse(String id){
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Can't find post"));
        return PostConverter.toPostResponse(post);
    }

    public List<Post> getPosts(){
        return postRepository.findAll();
    }

    public List<PostResponse> getPostResponses(){
        List<Post> posts = postRepository.findAll();
        return posts.stream()
                .map(PostConverter::toPostResponse)
                .collect(Collectors.toList());
    }


    public PostResponse updatePost(String id, PostRequest request){
        Post oldPost = getPost(id);
        Post newPost = PostConverter.toPost(request);
        newPost.setId(oldPost.getId());
        newPost.setMemberId(oldPost.getMemberId());

        postRepository.save(newPost);

        return PostConverter.toPostResponse(newPost);
    }

    public void deletePost(String id){
        postRepository.deleteById(id);
    }



}
