package blog.post;

import javax.validation.constraints.NotEmpty;

public class PostRequest {

    @NotEmpty(message = "Post title cannot be empty")
    private String title;

    @NotEmpty(message = "Post content cannot be empty")
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
