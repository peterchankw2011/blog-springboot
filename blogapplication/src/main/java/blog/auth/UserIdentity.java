package blog.auth;

import blog.member.Member;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class UserIdentity {

    private final MyUserDetails EMPTY_USER = new MyUserDetails(new Member());

    private MyUserDetails getMyUserDetails(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        return principal.equals("anonymousUser")
                ? EMPTY_USER
                : (MyUserDetails) principal;
    }

    public boolean isAnonymous() {
        return EMPTY_USER.equals(getMyUserDetails());
    }

    public String getId() {
        return getMyUserDetails().getId();
    }


    public String getEmail() {
        return getMyUserDetails().getUsername();
    }


}
