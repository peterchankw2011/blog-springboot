CREATE TABLE IF NOT EXISTS member (
    id serial PRIMARY KEY,
    profile_pic VARCHAR(255),
    self_intro VARCHAR(255),
    preferred_name VARCHAR(50),
    subscription_status boolean DEFAULT false,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS post (
    id serial PRIMARY KEY,
    member_id int REFERENCES member(id),
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);



CREATE TABLE IF NOT EXISTS comment (
    id serial PRIMARY KEY,
    member_id int REFERENCES member(id),
    post_id int REFERENCES post(id),
    content VARCHAR(255) NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);