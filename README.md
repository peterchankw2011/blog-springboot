# blog-springboot

This is a simple CRUD application to create blog posts with JWT authentication. 

## Steps

Run the Spring boot server, default port is 8080.

Get JWT token by calling post request to the route `\authenticate` with default user name *"blogger"* and password *"admin"*. 
```json
{
	"username":"blogger",
	"password":"admin"
}
```

 Sample JWT return
```json
{
  "jwt": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJibG9nZ2VyIiwiaWF0IjoxNjMzMzM0NzQ5LCJleHAiOjE2MzMzNzA3NDl9.qZIXqukS4OHoDvzVtSNI_i3iOuQKN8faJOR1K3OdzxY"
}
```

With the returned JWT, you can make requests by adding authorization to header:
```
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJibG9nZ2VyIiwiaWF0IjoxNjMzMzM0NzQ5LCJleHAiOjE2MzMzNzA3NDl9.qZIXqukS4OHoDvzVtSNI_i3iOuQKN8faJOR1K3OdzxY
```

Services available:
* GET `/blogs` list all blog posts
* GET `/blogs/{id}` get blog post by id
* POST `/blogs` create blog post
* PUT `/blogs/{id}` revise blog post by id
* DELETE `/blogs/{id}` delete blog post by id







